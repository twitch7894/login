import React from "react";
import { render } from "react-dom";
import Login from "./containers/login";
import "./styles/style.scss";

render(<Login />, document.getElementById("root"));
