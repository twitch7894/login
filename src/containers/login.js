import React from "react";
import Layout from "../components/login/layout";
import view from "../imgs/login.e3438b9d.jpg";

export default function Login() {
  return (
    <Layout>
      <div className="login">
        <div className="view">
          <img className="view-image" src={view} />
        </div>
        <div className="form">
          <div className="not-account">
            <p className="account">No tengo una cuenta?</p>
            <button className="started">Empezar</button>
          </div>
          <div className="form-text">
            <h2 className="text-sign-in">Iniciar session</h2>
            <p className="text-welcome">
              Bienvenido a nuestro Portal de Productores
            </p>
          </div>
          <p className="text-user">Usuario</p>
          <input className="input-user" type="text" />
          <p className="text-password">Contraseña</p>
          <input className="input-password" type="password" />
          <button className="login-button">Ingresar</button>
        </div>
      </div>
    </Layout>
  );
}
