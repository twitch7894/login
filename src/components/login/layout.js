import React from "react";
import logo from "../../imgs/logo-colon.jpg";

export default function Layout({ children }) {
  return (
    <div className="container">
      <div className="header">
        <img className="logo" src={logo} />
      </div>
      <div className="content">{children}</div>
    </div>
  );
}
